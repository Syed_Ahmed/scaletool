__author__ = 'Syed'
import os,re,Constants
from sqlalchemy import create_engine,update
from sqlalchemy.orm import sessionmaker
from Create_DataBase_Schema import failed_device_data,device_failure_reasons

input_dir=os.path.dirname(os.path.realpath(__file__))
engine = create_engine(Constants.DATABASE_ENGINE)
Session = sessionmaker(bind=engine)
session = Session()
resultSet = session.query(failed_device_data)
for result in resultSet:
    dictDHCP={"discover":0,"offer":0,"request":0,"ack":0}
    device_id=result.device_id
    mac_address=result.mac_address
    comments=''
    DHCPDISCOVER_search_string='DHCPDISCOVER[^\s]+'+" "+mac_address
    DHCPOFFER_search_string='DHCPOFFER[^\s]+ [^\s]+'+" "+mac_address
    DHCPREQUEST_search_string='DHCPREQUEST[^\s]+ [^\s]+'+" "+mac_address
    DHCPACK_search_string='DHCPACK[^\s]+ [^\s]+'+" "+mac_address
    for i in range(1,4,1):
        syslog_dir_path=input_dir+"\controller"+str(i)+"\syslog"
        for syslog_file in os.listdir(syslog_dir_path):
            with open(os.path.join(syslog_dir_path,syslog_file),"r") as fp:
                for line in fp:
                    if re.search(DHCPDISCOVER_search_string, line):
                        dictDHCP["discover"]+=1
                    elif re.search(DHCPOFFER_search_string,line):
                        dictDHCP["offer"]+=1
                    elif re.search(DHCPREQUEST_search_string,line):
                        dictDHCP["request"]+=1
                    elif re.search(DHCPACK_search_string,line):
                        dictDHCP["ack"]+=1
      
    if (not dictDHCP["discover"] and not dictDHCP["offer"] and not dictDHCP["request"] and not dictDHCP["ack"]):
        comments=comments+"device did not get IP address as DHCP server is down or not reachable"
    if (dictDHCP["discover"]>=3 and dictDHCP["offer"]>=3 and not dictDHCP["request"] and not dictDHCP["ack"]):
        comments=comments+"device did not get IP address as OVSvApp flows are not present"
    resultSet_comments=session.query(device_failure_reasons).filter(device_failure_reasons.device_id == device_id)[0]

    comments=resultSet_comments.comments+comments
    session = Session(autoflush=False)
    try:
        dfr=session.query(device_failure_reasons).filter(device_failure_reasons.device_id == device_id)[0]
        dfr.comments=comments
        session.add(dfr)
        session.commit()
    except :
        print "Exception"