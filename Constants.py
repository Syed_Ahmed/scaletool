'''
Created on Jan 19, 2016

@author: Syed
'''
#DB engine Constant
DATABASE_ENGINE="sqlite:///SCALABILITY.db"
#constants for Analyse_LOgs_OVSvApp
VM_CREATED_LOG="Handling event VM_CREATED for VirtualMachine{'vnics': [], 'uuid': "
DEVICE_CREATE_LOG="device_create notification for VM "
DEVICE_PROCESSED_LOG="device_create processed for VM "
GET_PORTS_RPC_LOG="Invoking get_ports_for_device rpc for device "
#Constants for Analyse_LOgs_OVS_DHCP
VLAN_ASSIGNMENT_LOG="as local vlan for net-id="