from sqlalchemy import *
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import relation,sessionmaker
import Constants

Base = declarative_base()
#insert data into failed_device_data taking input from 'port-details'
class failed_device_data(Base):
    __tablename__="failed_device_data"
    port_id=Column(String(36),nullable=False,primary_key=True)
    subnet_id=Column(String(36),nullable=False)
    ip=Column(String(15),nullable=False)
    device_id=Column(String(36),nullable=False)
    mac_address=Column(String(17),nullable=False)
    ovsvapp_binding_host=Column(String(36),nullable=False)
    network_id=Column(String(36),nullable=False)
    port_status=Column(String(6),nullable=False)

    def __init__(self,port_id,subnet_id,ip,device_id,mac_address,ovsvapp_binding_host,network_id, port_status):
        self.port_id=port_id;
        self.subnet_id=subnet_id;
        self.ip=ip
        self.device_id=device_id;
        self.mac_address=mac_address;
        self.ovsvapp_binding_host=ovsvapp_binding_host;
        self.network_id=network_id;
        self.port_status=port_status

#insert data into failed_device_times from 'allvms_time'
class failed_device_times(Base):
    __tablename__="failed_device_times"
    device_id=Column(String(36),nullable=False,primary_key=True)
    nova_spawn_time=Column(String(36),nullable=False)
    instance_active_time=Column(String(36),nullable=False)

    def __init__(self,device_id,nova_spawn_time,instance_active_time):
        self.device_id=device_id
        self.nova_spawn_time=nova_spawn_time
        self.instance_active_time=instance_active_time

#rows inserted from file analyse_Logs_OVSvApp
class vapp_device_times(Base):
    __tablename__= "vapp_device_times"
    get_ports_for_device_RPC_time=Column(String(36),nullable=True)
    device_id=Column(String(36),nullable=False,primary_key=True)
    vm_create_evnet_time=Column(String(36),nullable=True)
    device_create_RPC_start=Column(String(36),nullable=True)
    device_create_RPC_end=Column(String(36),nullable=True)

    def __init__(self,get_ports_for_device_RPC_time,device_id,vm_create_evnet_time,device_create_RPC_start,device_create_RPC_end):
        self.get_ports_for_device_RPC_time=get_ports_for_device_RPC_time
        self.device_id=device_id
        self.vm_create_evnet_time=vm_create_evnet_time
        self.device_create_RPC_start=device_create_RPC_start
        self.device_create_RPC_end=device_create_RPC_end

class device_failure_reasons(Base):
    __tablename__="device_failure_reasons"
    device_id=Column(String(36),nullable=False,primary_key=True)
    comments=Column(TEXT,nullable=True)

    def __init__(self,device_id,comments):
        self.device_id=device_id;
        self.comments=comments;

class failed_net_details(Base):
    __tablename__="failed_net_details"
    network=Column(String(15),nullable=False,primary_key=True)
    net_vm_count=Column(String(10),nullable=False)
    net_id=Column(String(36),nullable=False)

    def __init__(self,network,net_vm_count,net_id):
        self.network=network
        self.net_vm_count=net_vm_count
        self.net_id=net_id

class DHCP_Ready_Times(Base):
    __tablename__="DHCP_Ready_Times"
    network_id=Column(String(36),nullable=False,primary_key=True)
    DHCP_ready_time=Column(String(36),nullable=True)

    def __init__(self,network_id,DHCP_ready_time):
        self.network_id=network_id
        self.DHCP_ready_time=DHCP_ready_time

engine = create_engine(Constants.DATABASE_ENGINE)
Base.metadata.create_all(engine)
