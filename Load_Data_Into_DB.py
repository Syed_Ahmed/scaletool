__author__='Syed'
import os
import datetime,Constants
from sqlalchemy import create_engine
from Create_DataBase_Schema import failed_device_times,failed_device_data,failed_net_details
from sqlalchemy.orm import relation, sessionmaker
input_path = os.path.dirname(os.path.realpath(__file__)) + "/input"
ovs_log_dir=os.path.join(os.path.dirname(os.path.realpath(__file__)),"OVSvApp")
input_file_path_OVS=os.path.dirname(os.path.realpath(__file__))+"\output\device_OVS_host"
engine = create_engine(Constants.DATABASE_ENGINE)
Session = sessionmaker(bind=engine)
session = Session()
fp=open(os.path.join(input_path,'port-details'))
lines = []
for line in fp:                                               #line contains all the data of file
    lines.append(line.translate(None, ''.join(['  '])))         # error, TypeError: translate() takes exactly one argument (2 given)
fp.close()
for line in lines:
    items = line.split('|')
    print line
    items = items[1:-1]
    port_id=items[0]
    ovsvapp_binding_host=items[-3]
    d_id = items[2]
    network_id=items[-2]
    ip_dict = items[1].split('"')
    ip = ip_dict[-2]
    subnet_id=ip_dict[3]
    mac_address=items[3]
    port_status=items[-1]
    print port_id,subnet_id,ip,d_id,mac_address,ovsvapp_binding_host,network_id, port_status
    fdd=failed_device_data(port_id=port_id,subnet_id=subnet_id,ip=ip,device_id=d_id,mac_address=mac_address,ovsvapp_binding_host=ovsvapp_binding_host,network_id=network_id, port_status=port_status)
    try:
        session.add(fdd)
        session.commit()
    except:
        print "Exception"
date_format = '%Y-%m-%dT%H:%M:%SZ'
session = Session(autoflush=False)
with open(os.path.join(input_path, 'allvms_time')) as f:
    dets = f.read().splitlines()
    for line in dets:
        items = line.split('|')
        d_id = items[1].strip()
        temp = items[3].strip()
        nova_spawn_time = datetime.datetime.strptime(temp, date_format)
        temp = items[5].strip()
        instance_active_time = datetime.datetime.strptime(temp, date_format)
        fdt=failed_device_times(device_id=d_id,nova_spawn_time=nova_spawn_time,instance_active_time=instance_active_time)
        try:
            session.add(fdt)
            session.commit()
        except:
            print "Exception while writing into failed_device_times"
#insert data into failed net details from 'port-details'
input_path = os.path.dirname(os.path.realpath(__file__)) + "/input"
output_path = os.path.dirname(os.path.realpath(__file__)) + "/output"
details_file = 'port-details'
f_devices = set()
f_device_info = dict()
f_ip_info = dict()
fip_failures = set()        #collection of unique elements , ex : a = set(["Jake", "John", "Eric"])
pip_failures = set()
failed_macs = set()
failed_device_hosts = set()
dets = open(os.path.join(input_path, details_file))
lines = []
for line in dets:                                               #line contains all the date of file
    print line
    lines.append(line.translate(None, ''.join(['  '])))         # error, TypeError: translate() takes exactly one argument (2 given)
dets.close()
#f_macs = open(os.path.join(output_path, 'failed_macs'), "w+")
for line in lines:
    items = line.split('|')
    items = items[1:-1]         # list[start:stop] returns new list having objects between start and stop, L[-1] can be used to access the last item in a list
    ovs_host=items[-3]
    device = items[2]
    #f_device_OVS_host.write(device+"--"+ovs_host+"\n")
    f_devices.add(device)
    info_dict = {}
    info_dict['device'] = device
    info_dict['port'] = items[0]
    info_dict['network'] = items[-2]    # -2 refers to index position from right instead left, so -2 states second element from last
    info_dict['host'] = items[4]
    info_dict['mac'] = items[3]
    #f_macs.write(items[3] + " ---\n")
    failed_macs.add(items[3])
    failed_device_hosts.add(items[4])
    ip_dict = items[1].split('"')
    pip = ip_dict[-2]           #19.2.4.17
    info_dict['pip'] = pip
    pip_failures.add(pip)
    info_dict['fip'] = None
    f_device_info[device] = info_dict
    f_ip_info[pip] = info_dict
    f_ovs_host_info=f_ip_info
nets = set()
net_vm_count = {}
net_ip_list = {}
net_ids = {}
ip_devices = {}
for ip in sorted(pip_failures):
    ip_devices[ip] = f_ip_info[ip]['device']   #dict within dict
    octets = ip.split('.')
    octets[3] = 0
    net = '.'.join(str(x) for x in octets)
    if net in net_vm_count:
        cur_count = net_vm_count[net]
        ip_list = net_ip_list[net]
        ip_list.append(ip)
        net_vm_count[net] = cur_count + 1
        net_ip_list[net] = ip_list
    else:
        nets.add(net)
        net_vm_count[net] = 1
        net_ip_list[net] = [ip]
        net_ids[net] = f_ip_info[ip]['network']
net_counts = []
net_ips = []
session = Session(autoflush=False)
for net in sorted(nets):
    fnd=failed_net_details(network=net,net_vm_count=str(net_vm_count[net]),net_id=net_ids[net])
    net_counts.append(net + " --- " + str(net_vm_count[net]) + "--- " + net_ids[net])
    net_ips.append(net + "---" + str(net_vm_count[net]) + str(net_ip_list[net]))
    try:
        session.add(fnd)
        session.commit()
    except:
        print "Exception while writing into failed_net_details"
