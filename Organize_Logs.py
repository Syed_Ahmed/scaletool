__author__ = 'Syed'

import shutil
import os
import gzip
import fnmatch
import glob
import time

start_time=time.time()
controller_count = 3

input_controller_dir_path = os.path.join(os.path.dirname(os.path.realpath(__file__)),'logs')
controller1_dir_path = os.path.dirname(os.path.realpath(__file__))
controller2_dir_path = os.path.dirname(os.path.realpath(__file__))
controller3_dir_path = os.path.dirname(os.path.realpath(__file__))
controllers_dir_list = [controller1_dir_path, controller2_dir_path, controller3_dir_path]
OVSvApp_dir_path=os.path.join(os.path.dirname(os.path.realpath(__file__)),'OVSvApp')
OVSvAPP_input_logs_path=os.path.join(os.path.dirname(os.path.realpath(__file__)),'logs')+"/ovs-vapps"

def decompress_logs(source_dir, dest_dir, pattern):
    for src_name in glob.glob(os.path.join(source_dir, pattern)):
        base = os.path.basename(src_name)
        dest_name = os.path.join(dest_dir, base[:-3])
        with gzip.open(src_name, 'rb') as infile:
            with open(dest_name, 'w') as outfile:
                for line in infile:
                    outfile.write(line)
i = 1
for x in controllers_dir_list:
    str_i = str(i)
    ctrl = x + "/controller" + str_i
    try:
        os.stat(ctrl)
    except:
        os.mkdir(ctrl)
    try:
        os.stat(ctrl + "/syslog")
    except:
        os.mkdir(ctrl + "/syslog")
    try:
        os.stat(ctrl + "/DHCP")
    except:
        os.mkdir(ctrl + "/DHCP")
    try:
        os.stat(ctrl + "/OVS")
    except:
        os.mkdir(ctrl + "/OVS")
    try:
        os.stat(ctrl + "/L3")
    except:
        os.mkdir(ctrl + "/L3")
    try:
        os.stat(ctrl + "/serverlog")
    except:
        os.mkdir(ctrl + "/serverlog")
    i = i + 1
try:
    os.stat(os.path.join(os.path.dirname(os.path.realpath(__file__)),'OVSvApp'))
except:
    os.mkdir(os.path.join(os.path.dirname(os.path.realpath(__file__)),'OVSvApp'))
if (os.path.isdir(input_controller_dir_path + "/-mgmt")):
    os.rmdir(input_controller_dir_path + "/-mgmt")
j = 1
dir_List=['cc1','cc2','cmc']
for dir in os.listdir(OVSvAPP_input_logs_path):
    try:
        os.stat(os.path.join(OVSvApp_dir_path,dir))
    except:
        os.mkdir(os.path.join(OVSvApp_dir_path,dir))
    temp_dir=os.path.join(os.path.join(OVSvAPP_input_logs_path,dir),'hpvcn-agent')
    for file in os.listdir(temp_dir):
        if fnmatch.fnmatch(file,'agent.log'):
            shutil.copy(os.path.join(temp_dir,file),os.path.join(os.path.join(OVSvApp_dir_path,dir),file))
        elif fnmatch.fnmatch(file,'agent.log.1'):
            shutil.copy(os.path.join(temp_dir,file),os.path.join(os.path.join(OVSvApp_dir_path,dir),file))
        decompress_logs(temp_dir,os.path.join(OVSvApp_dir_path,dir),'agent.log.*.gz')
for dir in dir_List:
    ctrlj = 'controller' + str(j)
    temp_dest_dir = os.path.join(controller1_dir_path, ctrlj)
    decompress_logs(os.path.join(input_controller_dir_path, dir), os.path.join(temp_dest_dir, 'syslog'), '*.gz')

    for file in os.listdir(os.path.join(input_controller_dir_path, dir)):
        if fnmatch.fnmatch(file, 'syslog'):
            src_dir_path = os.path.join(input_controller_dir_path, dir)
            shutil.copy(src_dir_path + '/syslog', os.path.join(temp_dest_dir, 'syslog') + '/syslog')
        elif fnmatch.fnmatch(file,'syslog.1'):
            src_dir_path = os.path.join(input_controller_dir_path, dir)
            shutil.copy(src_dir_path + '/syslog.1', os.path.join(temp_dest_dir, 'syslog') + '/syslog.1')

    neutron_path = os.path.join(input_controller_dir_path, dir)
    neutron_dir = os.path.join(neutron_path, 'neutron')

    decompress_logs(neutron_dir, os.path.join(temp_dest_dir, 'DHCP'), 'neutron-dhcp-agent.log.*.gz')
    decompress_logs(neutron_dir, os.path.join(temp_dest_dir, 'OVS'), 'neutron-openvswitch-agent.log.*.gz')
    decompress_logs(neutron_dir, os.path.join(temp_dest_dir, 'L3'), 'neutron-l3-agent.log.*.gz')
    decompress_logs(neutron_dir, os.path.join(temp_dest_dir, 'serverlog'), 'neutron-server.log.*.gz')

    for file in os.listdir(neutron_dir):
        if fnmatch.fnmatch(file, 'neutron-dhcp-agent.log'):
            shutil.copy(neutron_dir + '/neutron-dhcp-agent.log',
                        os.path.join(temp_dest_dir, 'DHCP') + '/neutron-dhcp-agent.log')
        elif fnmatch.fnmatch(file, 'neutron-dhcp-agent.log.1'):
            shutil.copy(neutron_dir + '/neutron-dhcp-agent.log.1',
                        os.path.join(temp_dest_dir, 'DHCP') + '/neutron-dhcp-agent.log.1')
        elif fnmatch.fnmatch(file, 'neutron-openvswitch-agent.log'):
            shutil.copy(neutron_dir + '/neutron-openvswitch-agent.log',
                        os.path.join(temp_dest_dir, 'OVS') + '/neutron-openvswitch-agent.log')
        elif fnmatch.fnmatch(file, 'neutron-openvswitch-agent.log.1'):
            shutil.copy(neutron_dir + '/neutron-openvswitch-agent.log.1',
                        os.path.join(temp_dest_dir, 'OVS') + '/neutron-openvswitch-agent.log.1')
        elif fnmatch.fnmatch(file, 'neutron-l3-agent.log'):
            shutil.copy(neutron_dir + '/neutron-l3-agent.log',
                        os.path.join(temp_dest_dir, 'L3') + '/neutron-l3-agent.log')
        elif fnmatch.fnmatch(file, 'neutron-l3-agent.log.1'):
            shutil.copy(neutron_dir + '/neutron-l3-agent.log.1',
                        os.path.join(temp_dest_dir, 'L3') + '/neutron-l3-agent.log.1')
        elif fnmatch.fnmatch(file, 'neutron-server.log'):
            shutil.copy(neutron_dir + '/neutron-server.log',
                        os.path.join(temp_dest_dir, 'serverlog') + '/neutron-server.log')
        elif fnmatch.fnmatch(file, 'neutron-server.log.1'):
            shutil.copy(neutron_dir + '/neutron-server.log',
                        os.path.join(temp_dest_dir, 'serverlog') + '/neutron-server.log.1')
    j = j + 1
    if j > (controller_count + 1):
        break
print "\n processed logs of %s controllers \n Logs has been successfully organised"%(j-1)
exc_time=time.time()-start_time
print "Execution time is %s seconds" %exc_time
