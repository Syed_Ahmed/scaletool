__author__ = 'Syed'

import datetime,Constants
from sqlalchemy import create_engine,update
from sqlalchemy.orm import sessionmaker
from Create_DataBase_Schema import failed_net_details,failed_device_data,device_failure_reasons,vapp_device_times,DHCP_Ready_Times

engine = create_engine(Constants.DATABASE_ENGINE)
Session = sessionmaker(bind=engine)
session = Session()
resultSet = session.query(failed_device_data)
for r in resultSet:
    comments=''
    print r.device_id
    print r.network_id
    resultSet_comments=session.query(device_failure_reasons).filter(device_failure_reasons.device_id == r.device_id)[0]
    session = Session()
    resultSet_VM_create_time=session.query(vapp_device_times).filter(vapp_device_times.device_id == r.device_id)[0]
    session = Session()
    resultSet_vlan_assignment_time=session.query(DHCP_Ready_Times).filter(DHCP_Ready_Times.network_id == r.network_id)[0]
    if not resultSet_vlan_assignment_time:
        comments=resultSet_comments.comments+"there was no vlan assigned for the netowrk to which this device is belonged to\n"
    elif resultSet_vlan_assignment_time.DHCP_ready_time!='' and  resultSet_VM_create_time.vm_create_evnet_time!='':
        print resultSet_VM_create_time.vm_create_evnet_time
        T1 = datetime.datetime.strptime(resultSet_VM_create_time.vm_create_evnet_time, '%Y-%m-%d %H:%M:%S.%f')
        print resultSet_vlan_assignment_time.DHCP_ready_time
        T2=datetime.datetime.strptime(resultSet_vlan_assignment_time.DHCP_ready_time, '%Y-%m-%d %H:%M:%S.%f')
        diff=T2 - T1 #T1 is VM_create_time and T2 is vlan_assignment time
        print "diff is :"
        print diff.total_seconds()
        if (diff.total_seconds() > 120):
            comments=comments+"diff between vm_create_time and vlan assignment time is more than 120 seconds\n"
    comments=resultSet_comments.comments+comments
    stmt=update(device_failure_reasons).where(device_failure_reasons.device_id==r.device_id).values(comments=comments)
    session = Session(autoflush=False)
    try:
        dfr=session.query(device_failure_reasons).filter(device_failure_reasons.device_id == r.device_id)[0]
        dfr.comments=comments
        session.add(dfr)
        session.commit()
    except :
        print "Exception"
