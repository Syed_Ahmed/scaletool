__author__ = 'Syed'

import os
import Constants
from sqlalchemy import create_engine
from Create_DataBase_Schema import vapp_device_times,device_failure_reasons,failed_device_data
from sqlalchemy.orm import sessionmaker
input_path = os.path.dirname(os.path.realpath(__file__)) + "/input"
ovs_log_dir=os.path.join(os.path.dirname(os.path.realpath(__file__)),"OVSvApp")
input_file_path_OVS=os.path.dirname(os.path.realpath(__file__))+"\output\device_OVS_host"

engine = create_engine(Constants.DATABASE_ENGINE)
Session = sessionmaker(bind=engine)
session = Session()
ovs_log_dir=os.path.dirname(os.path.realpath(__file__))+"\OVSvApp"
## input_file_path is device_OVS_host file which contains the device id and associated OVSvApp host
session = Session()
resultSet = session.query(failed_device_data)    
resultSet = session.query(failed_device_data)               #added new line
for result in resultSet:
    search_string_get_ports_for_device_RPC=''
    search_string_VM_CREATED=''
    search_string_device_create=''
    search_String_device_processed=''
    get_ports_for_device_RPC_time=''
    device_id=''
    vm_create_evnet_time=''
    device_create_RPC_start=''
    device_create_RPC_end=''
    comments=''
    migrated_OVSvApp_Host=''
    #items = line.split('--')
    d_id= result.device_id                  #items[0]
    ovs_host= result.ovsvapp_binding_host              #items[1]
    search_string_VM_CREATED=Constants.VM_CREATED_LOG+d_id
    search_string_device_create=Constants.DEVICE_CREATE_LOG+d_id
    search_String_device_processed=Constants.DEVICE_PROCESSED_LOG+d_id
    search_string_get_ports_for_device_RPC=Constants.GET_PORTS_RPC_LOG+d_id
    actual_ovs_dir=os.path.join(ovs_log_dir,ovs_host)
    found_VM_CREATED_Event_time=False
    found_device_create_Event_time=False
    found_device_processed_Event_time=False
    found_get_ports_for_device_rpc=False
    device_id=d_id
    for dir in os.listdir(ovs_log_dir):
        for file in os.listdir(os.path.join(ovs_log_dir,dir)):
            fileptr=open(os.path.join(os.path.join(ovs_log_dir,dir),file),"r")
            for line in fileptr.readlines():
                if search_string_get_ports_for_device_RPC in line:
                    found_get_ports_for_device_rpc=True
                    migrated_OVSvApp_Host=dir
                    temp_items=line.split(" ")
                    get_ports_for_device_RPC_time=temp_items[0]+" "+temp_items[1]
                    break
            if found_get_ports_for_device_rpc:
                break
        if found_get_ports_for_device_rpc:
            break
    if ovs_host==migrated_OVSvApp_Host:
        print "there was no migration for device: "+d_id
    if ovs_host!=migrated_OVSvApp_Host:
        print "Actual owner of device was :"+migrated_OVSvApp_Host+"and current host is :"+ovs_host
    for file in os.listdir(actual_ovs_dir):
        f2=open(os.path.join(actual_ovs_dir,file),"r")
        for line in f2.readlines():
            if search_string_VM_CREATED in line:
                found_VM_CREATED_Event_time=True
                items1=line.split()
                vm_create_evnet_time=items1[0]+" "+items1[1]
                break
        if found_VM_CREATED_Event_time:
            break
    for file in os.listdir(actual_ovs_dir):
        f2=open(os.path.join(actual_ovs_dir,file),"r")
        for line in f2.readlines():
            if search_string_device_create in line:
                found_device_create_Event_time=True
                items2=line.split()
                device_create_RPC_start=items2[0]+" "+items2[1]
                break
        if found_device_create_Event_time:
            break
    for file in os.listdir(actual_ovs_dir):
        f2=open(os.path.join(actual_ovs_dir,file),"r")
        for line in f2.readlines():
            if search_String_device_processed in line:
                found_device_processed_Event_time=True
                items3=line.split()
                device_create_RPC_end=items3[0]+" "+items3[1]
                break
        if found_device_processed_Event_time:
            break
    if not found_get_ports_for_device_rpc:
        comments=comments+"there was no RPC call for get_ports_for_device\n"
    if ovs_host==migrated_OVSvApp_Host:
        comments=comments+ "there was no migration for the device"+"\n"

    if not found_VM_CREATED_Event_time:
        comments=comments+"event VM_CREATED was not received"+"\n"
    if not found_device_create_Event_time:
        comments=comments+" no notification of \"device_create\" for the device"+"\n"
    if not found_device_processed_Event_time:
        comments=comments+"device_create RPC was not processed completely\n"  #"there was no \"device_create\" processed event for the device"+"\n"
    vdt=vapp_device_times(get_ports_for_device_RPC_time=get_ports_for_device_RPC_time,device_id=device_id,vm_create_evnet_time=vm_create_evnet_time,device_create_RPC_start=device_create_RPC_start,device_create_RPC_end=device_create_RPC_end)
    dfr=device_failure_reasons(device_id=device_id,comments=comments)
    try:
        session.add(vdt)
        session.add(dfr)
        session.commit()
    except:
        print "Exception"