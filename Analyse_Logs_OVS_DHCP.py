__author__ = 'Syed'
import os,Constants
from datetime import datetime
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from Create_DataBase_Schema import failed_net_details,DHCP_Ready_Times
#
#write vlan assignment time to DHCP_Ready_Times
#
input_log_dir=os.path.dirname(os.path.realpath(__file__))
engine = create_engine(Constants.DATABASE_ENGINE)
Session = sessionmaker(bind=engine)
session = Session()
resultSet = session.query(failed_net_details)
for result in resultSet:
    found=False
    network_id=result.net_id
    DHCP_ready_time_list=[]
    searchString=Constants.VLAN_ASSIGNMENT_LOG+network_id
    for i in range(1,4,1):
        OVS_logs_path=input_log_dir+"\controller"+str(i)+"\OVS"
        for f in os.listdir(OVS_logs_path):
           Actual_File_Path=os.path.join(OVS_logs_path,f)
           with open(Actual_File_Path,'r') as fptr:
               for line in fptr:
                    if searchString in line:
                        #print "found"
                        lineContents=line.split(" ")
                        DHCP_ready_time=lineContents[0]+" " +lineContents[1]
                        #print DHCP_ready_time
                        str_to_date=datetime.strptime(DHCP_ready_time, "%Y-%m-%d %H:%M:%S.%f")
                        DHCP_ready_time_list.append(str_to_date)
                        print Actual_File_Path + "\n"
                        break
           if found:
               break
    DHCP_ready_time_list.sort()
    print "for network id: " + network_id
    if DHCP_ready_time_list:
        drt=DHCP_ready_time_list[0]
    else:
        print "no vlan assignment for the network "+network_id
        drt=''
    dhcprt=DHCP_Ready_Times(network_id=network_id,DHCP_ready_time=str(drt))
    try:
        session.add(dhcprt)
        session.commit()
    except:
        print "Exception"
